/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class Square extends Rectangle
{
    @Override
    public void setHeight(int height)
    {
        super.setHeight(height);
        super.setWidth(height);
    }
    
    @Override
    public void setWidth(int width)
    {
        super.setWidth(width);
        super.setHeight(width);
    }
    
    @Override
    public int getArea()
    {
        return super.getHeight() * super.getWidth();
    }
    
    public int getParameter(Square s, int width, int height)
    {
        s.setWidth(width);
        s.setHeight(height);
        return s.getArea();
    }
}
