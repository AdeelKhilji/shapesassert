/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class Rectangle 
{
    private int width;
    private int height;
    
    public Rectangle(){}
    
    public Rectangle(int width, int height)
    {
        this.width = width;
        this.height = height;
    }
    public void setWidth(int width)
    {
        this.width = width;
    }
    public int getWidth()
    {
        return this.width;
    }
    public void setHeight(int height)
    {
        this.height = height;
    }
    public int getHeight()
    {
        return this.height;
    }
    
    public int getArea()
    {
        return this.width * this.height;
    }
    public int getParameter(Rectangle r, int width, int height)
    {
        r.setWidth(width);
        r.setHeight(height);
        return r.getArea();
    }
    
}
