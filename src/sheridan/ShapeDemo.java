/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import static org.junit.Assert.assertEquals;
/**
 *
 * @author Adeel Khilji
 */
public class ShapeDemo 
{
    public static void calculateArea(Rectangle r)
    {
        r.setWidth(2);
        r.setHeight(3);
        
        assertEquals("Area calculation is incorrect", 6, r.getArea());
        
    }
    
    public static void main(String[] args)
    {
        ShapeDemo.calculateArea(new Rectangle());
        
        ShapeDemo.calculateArea(new Square());
    }
}
